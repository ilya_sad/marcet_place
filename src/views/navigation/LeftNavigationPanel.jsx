import React, { Fragment } from 'react';
import ButtonGroup from '../../components/button-group/ButtonGroup';
import Button from '../../components/button-group/ButtonGroup';
import Icon from '../../components/icon/Icon.jsx';
import { useState } from 'react';
import menuIcon from './icons/menu.svg'
import printerIcon from './icons/printer.svg'
import arrowLeftIcon from './icons/arrow_left.svg'
import arrowRightIcon from './icons/arrow_right.svg'
import arrowRefreshIcon from './icons/refresh.svg'
import messageIcon from './icons/message.svg'
import './NavigationPanel.css'

const LeftNavigationPanel = () => {
	const [count, setCount] = useState( 0 );
	return ( <Fragment className="btn-group-top">
		<ButtonGroup className="btn-group-top vertical">
			<Icon svg={menuIcon} onClick={() => {
					setCount( count + 1 );
					console.log( count );
				}}/>
			<Icon svg={printerIcon} className="icon"/>
			<Icon svg={arrowLeftIcon} className="icon"/>
			<Icon svg={arrowRightIcon} className="icon"/>
			<Icon svg={arrowRefreshIcon} className="icon"/>
			<Icon svg={messageIcon} className="icon"/>
		</ButtonGroup>
	</Fragment> )
};

export default LeftNavigationPanel;
