/* eslint-disable */
import React, { Fragment } from 'react';
import TopNavigationPanel from '../navigation/TopNavigationPanel.jsx'
import LeftNavigationPanel from '../navigation/LeftNavigationPanel.jsx'

import Example from './Example.js'
// import ButtonGroup from '../../components/button-group/ButtonGroup';
// import Icon from '../../components/icon/Icon.jsx';
import { useState } from 'react';
import './Home.css';

const Home = () => {
	const [count, setCount] = useState( 0 );
	return ( <Fragment>
		<div className="A">
			<TopNavigationPanel className="A1"></TopNavigationPanel>
			<LeftNavigationPanel></LeftNavigationPanel>
		</div>

		<Example></Example>
	</Fragment> )
};

export default Home;
/* eslint-enable */
