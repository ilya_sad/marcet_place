import React from 'react';
import PropTypes from 'prop-types';
import { useState } from 'react';
import classNames from 'classnames';
import './Icon.css';
const Icon = ( {
	svg,
	className,
	onClick,
	altName,
	...attrs
} ) => {

	// const classes = classNames( { func: onClick } );
	const link = `${ svg }`;
	// #svgView(viewBox(0,0,128,128));
	return ( <img className={className} src={link} alt={altName} onClick={onClick}/> )
};
Icon.propTypes = {
	name: PropTypes.string,
	className: PropTypes.string,
	size: PropTypes.number,
	onClick: PropTypes.func,
	disabled: PropTypes.bool
};
Icon.defaultProps = {
	name: '',
	className: '',
	size: null,
	onClick: null,
	disabled: false
};

export default Icon;
