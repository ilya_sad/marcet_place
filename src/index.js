import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import Button from './components/button/Button.js';
import Home from './views/home/Home.jsx';

ReactDOM.render( <Home/>, document.getElementById( 'root' ) );
